This is a list of contributors to this package:

  * cinnamon <cinnamon250 at outlook dot com>

If you have done something for this package, please send me an email with your 
name and what you have done or send a patch to this file when posting an patch 
to this package.
