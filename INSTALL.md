To install this package you have tree options:

1. Use Bakefile

----- TODO: Read the bakefile documentation in order to know more about it and
						fill this section.


2. Use CMake:

2.1. In Unix:

	a) Go to the directory where you extracted the source package;

	b) Create a directory to build barbara (since CMake likes out-of-source
     builds), for e.g.:

		$mkdir build

	c) Go to that directory and execute ccmake (or if you have it cmake-gui):

		$ccmake -G "Unix Makefiles" ..\

    Then ccmake will check if the required dependencies are available, and 
		create the makefile for you to build the package.

    Note that option G specified that Unix based makefiles are going to be
		generated (read cmake documentation if you want to use something else),
    and that the source directory is one level up (..\), you need to change
		that to the relative location of your source folder. If you use cmake-gui
		you will choose both the relative source location and the target build
		system in the gui.

	d) If everything went well, build the program using your system make program:
		
		$make

	e) Optionally you can run the tests, like this:

		$make test

	f) And install the program automatically if you want, like this:

		$make install

2.2. In Windows using MSVC:

	a) Install cmake from it's webpage, then run cmake-gui from either start menu,
		 or by going to the directory where you install cmake, then to bin/cmake-gui.exe

	b) Create a directory to build the program, a diferent one than where you
		 extracted the source files

  c) Input the directory where you installed the source and the build directory
		 that you created in the second step, in the cmake-gui and click configure

	d) Choose the type of makefile to generate, in this case the entry that
		 contains MSVC

	e) After the configure fill in where you have the necessary dependencies,
		 and press configure again (do this step, until you have no more red lines
		 after pressing configure)

	f) Press generate, to generate the MSVC projects

	g) Go to the directory that you created in step b, and click the generated project
		 file in order to build the program

		Note that you can also "build" the pseudo-projects test and install, in order
		to test the compiled binaries and install them repectivily.

2.3. In Windows using mingw32 and cmd:

	a) Do the steps a), b) and c) from section 2.2., then do the step d) but instead
		 of choosing the MSVC, choose MINGW makefiles, continue and execute step e)

	b) Press generate, to create the mingw makefiles.

	c) Launch a cmd session, that has the mingw stuff in it's path, go to the build
		 directory that you created

		 $cd path\to\build

	d) Run mingw make to build the files, followed by make test and/or make install
		 if you want to test and/or install the files automatically.

2.4. In Windows using msys:

	a) Do the steps a), b) and c) from section 2.2, then do the step d) but choosing
		 msys makefiles, then continue and execute step e)

	b) Press generate, to create the msys makefiles.

	c) Launch a msys shell, go to the build directory you created and execute make
		 to build the files, followed by make test and/or make instal if you want to
		 test and/or install the files automatically.

CMake Note: Since CMake is actually a meta-makefile it can create projects for
other platforms/compilers, so you can read CMake documentation if you want to
know if cmake supports your compiler (if it isn't in the list), and how to build
the project for it.

Please read the readme to know which platforms are supported by barbara.

3. Use the native build scripts

  In the root directory there are other build scripts that you may use to
  compile barbara, if you want to use them read bellow for more information.

----- TODO: After adding another build script, add here the relevant
			instructions for it.

	Note that these files aren't generated regularly but the ones that exist are
	generated using either cmake or bakefile.
