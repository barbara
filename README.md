# README #

This is an sample readme file for the program NNNNN, it contains instructions
on what this software does, how to install it, where to find the documentation,
where to find the software downloads, where to find the mailing-list, issue 
tracker and repository.

## Description ##

Insert an brief description of what the software does in here, it should describe
everything that you can do with it, in two or three paragraphs.
Then, follow it by an enumeration of some of the killer features of this software.

  * Feature N;

## License ##

This software is licensed under the GNU General Public License version 3 or 
later.

or 

This software is licensed under the GNU Lesser General Public License 
version 3 or later.

or

This software is licensed under the GNU Affero General Public License version 3 
or later.

The images, videos, sounds and documentation of this software are licensed under 
the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported 
License. To view a copy of this license, 
visit http://creativecommons.org/licenses/by-nc-sa/3.0/.

## Install ##

In order to know how to install this software please read the INSTALL.md file.

## Documentation ##

The documentation of this software is available either on the website or
if you have cloned the repository, in the repository wiki.

Also, there is some documentation available in the doc directory, please
read the INSTALL.md file in order to know how to generate the documentation
in the pdf, html or other format as you wish.

## Contact ##

In order to contact the software authors, please fo to the software website, given
bellow:

Website URL: <URL_OF_THE_PROJECT_WEBSITE>

Mailing-List: <URL_OF_THE_PROJECT_MAILING_LIST>

Bug/Issue or Feature Tracker: <URL_OF_THE_PROJECT_BUG_OR_ISSUE_OR_FEATURE_TRACKER>

Documentation: <URL_OF_THE_PROJECT_DOCUMENTATION>

Repository: <URL_OF_THE_REPOSITORY_HUMAN_READABLE>
