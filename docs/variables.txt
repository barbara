@PROJECT_NUMBER@ Project Version
@PROJECT_LOGO@ Project Logo
@DOC_OUTPUT@ Documentation Output
@SRC_PATH@ Source path separated by spaces

@HAS_HTML@ Produce HTML Output
@HTML_OUTPUT@ HTML Documentation output

@HAS_DYN_HTML@ HTML is dynamic

@HAS_OSX@ HTML is to be used by XCode

@HAS_HTMLHELP@ Produce HTMLHELP Output
@CHM_OUTPUT@ CHM file output
@HHC_LOCATION@ HHC compiler location

@HAS_QTHELP@ Produce QT Help
@QHC_OUTPUT@ QT Help output
@QHC_PATH@ QT Help compiler location

@HAS_ECLIPSEHELP@ Produce eclipse help

@HAS_LATEX_DOC@ Produce Latex documentation
@LATEX_OUTPUT@ Latex file output
@LATEX_PATH@ Latex processor location
@MAKEINDEX_PATH@ Makeindex processor location
@HAS_PDFLATEX@ Use pdflatex instead of latex

@HAS_RTF@ Generate RTF output
@RTF_OUTPUT@ RTF file location

@HAS_MAN@ Generate MAN Documentation
@MAN_OUTPUT@ Man documentation output

@HAS_XML@ Generate XML documentation

@INCLUDE_DIR@ Specify the include directory

@HAS_DOT@ Specify the dot location
@DOT_PATH@ Dot processor location
